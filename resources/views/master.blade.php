<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My Task List</title>
 
    <!-- Bootstrap CSS File  -->
     <!--<link rel="stylesheet" type="text/css" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}"/> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
</head>
<body>
 
<div class="container">
 
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="">Task List CRUD Application</a>
            </div>
            <ul class="nav navbar-nav">
 
            </ul>
        </div>
    </nav>
 
    <head>
        <h1></h1>
    </head>
 
    @yield('content')
 
</div>
</body>
</html>