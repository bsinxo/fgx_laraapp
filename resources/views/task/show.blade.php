@extends('master')
 
@section('content')
 
    <div class="panel panel-default">
        <div class="panel-heading">
            View Task
        </div>
        <br/>
        <div class="panel-body">
            <div class="pull-right">
                <a class="btn btn-info" href="{{ route('tasks.index')  }}">Back</a>
            </div>
            <div class="form-group">
                <strong>Task Name: </strong> {{ $task->name  }}
            </div>
            <div class="form-group">
                <strong>Task Description: </strong> {{ $task->description  }}
            </div>
        </div>
    </div>
 
@endsection